/*
Author :  AA-Team - http://themeforest.net/user/AA-Team
*/
AA_Mega_Menu = (function ($) {
    "use strict";
	
	// globals vars
	var settings = {
		trigger: "hover",
		showSpeed: 350,
		hideSpeed: 350,
		showDelay: 0,
		hideDelay: 0,
		effect: "fade",
		align: "left",
		responsive: true,
		animation: "none",
		indentChildren: true,
		indicatorFirstLevel: "<i class=\"fa fa-sort-desc\"></i>",
		indicatorSecondLevel: "<i class=\"fa fa-sort-desc\"></i>",
		scrollable: true,
		scrollableMaxHeight: 400,
		menuWidthParent: null
	},
	mobileWidthBase = 768,
	bigScreenFlag = 1800, // a number greater than "mobileWidthBase"
	smallScreenFlag = 200; // a number less than "mobileWidthBase";
	
	
	$(".aa-mega-menu-wrap").each(function(){
		
		var menu_container 	= $(this),
			menu 			= $(menu_container).children(".aa-mega-menu"),
			menu_li 		= $(menu).find("li"),
			showHideButton;

		// default width parent
		settings.menuWidthParent = menu_container.parents('.container').eq(0);
		
		// update the settings
		update_settings();
		
		// call the init function
		init();
		
		function update_settings()
		{
			var options = {};
			
			var data = menu.data();
			for(var i in data){
  				options[i] = data[i];
  			}
			
			$.extend( settings, options );
		}
		
		function add_indicators()
		{
			menu.children("li").children("a").each( function(){
				if( $(this).siblings(".aa-mega-sub-menu, .aa-mega-menu-wrap").length > 0 ){
					$(this).append("<span class='indicator'>" + settings.indicatorFirstLevel + "</span>");
				}
			});
			
			menu.find(".aa-mega-sub-menu").children("li").children("a").each( function(){
				if( $(this).siblings(".aa-mega-sub-menu").length > 0 ){
					$(this).append("<span class='indicator'>" + settings.indicatorSecondLevel + "</span>");
				}
			});
		}
		
		// shows a dropdown
		function show_dropdown( item )
		{
			var children = $(item).children(".aa-mega-sub-menu, .aa-mega-menu-wrap");
			if( settings.effect == "fade" ){
				children.stop(true, true).delay(settings.showDelay).fadeIn(settings.showSpeed).addClass(settings.animation);
			}
			else{
				children.stop(true, true).delay(settings.showDelay).slideDown(settings.showSpeed).addClass(settings.animation);
			}
		}
		
		// hides a dropdown
		function hide_dropdown(item)
		{
			var children = $(item).children(".aa-mega-sub-menu, .aa-mega-menu-wrap");
			if( settings.effect == "fade" ){
				children.stop(true, true).delay(settings.hideDelay).fadeOut(settings.hideSpeed).removeClass(settings.animation);
			}
			else {
				children.stop(true, true).delay(settings.hideDelay).slideUp(settings.hideSpeed).removeClass(settings.animation);
			}
			
			children.find(".aa-mega-sub-menu, .aa-mega-menu-wrap").stop(true, true).delay(settings.hideDelay).fadeOut(settings.hideSpeed);
		}
		
		// Fix the submenu on the right side
		function fix_submenu_right()
		{
			var submenus = $(menu).children("li").children(".aa-mega-sub-menu, .aa-mega-menu-wrap");
			if($(window).innerWidth() > mobileWidthBase){
				var menu_width = $(menu_container).outerWidth(true);
				for(var i = 0; i < submenus.length; i++){
					if( $(submenus[i]).parent("li").position().left + $(submenus[i]).outerWidth() > menu_width ){
						$(submenus[i]).css("right", '21px');
					}
					else{
						if( 
							menu_width == $(submenus[i]).outerWidth() || 
							(menu_width - $(submenus[i]).outerWidth()) < 20 
						){
							$(submenus[i]).css("left", 0);			
						}
						
						if(
							$(submenus[i]).parent("li").position().left + $(submenus[i]).outerWidth() < menu_width
						){
							$(submenus[i]).css("right", "auto");
						}
					}
				}
			}
		}
		
		// landscape mode
		function work_landscape_mode()
		{
			$(menu).find(".aa-mega-sub-menu, .aa-mega-menu-wrap").hide(0);
			if(navigator.userAgent.match(/Mobi/i) || window.navigator.msMaxTouchPoints > 0 || settings.trigger == "click"){
				$(".aa-mega-menu-wrap > ul > li > a, .aa-mega-menu-wrap ul.aa-mega-sub-menu li a").bind("click touchstart", function(e){
					e.stopPropagation(); 
					e.preventDefault();
					$(this).parent("li").siblings("li").find(".aa-mega-sub-menu, .aa-mega-menu-wrap").stop(true, true).fadeOut(300);
					 
					if($(this).siblings(".aa-mega-sub-menu, .aa-mega-menu-wrap").css("display") == "none"){
						show_dropdown($(this).parent("li"));
						return false; 
					}
					else{
						hide_dropdown($(this).parent("li"));
						
						if( $(this).parent("li > a > .indicator ").size() > 0  ){ 
							return false;
						}
					}
					window.location.href = $(this).attr("href");
				});
				$(document).bind("click.menu touchstart.menu", function(ev){
					if($(ev.target).closest(".aa-mega-menu").length == 0){
						$(".aa-mega-menu-wrap").find(".aa-mega-sub-menu, .aa-mega-menu-wrap").fadeOut(300);
					}
				});
			}
			else{
				$(menu_li).bind("mouseenter", function(){
					show_dropdown(this);
				}).bind("mouseleave", function(){
					hide_dropdown(this);
				});
			}
		}
		
		// hide the bar to show/hide menu items on mobile
		function hide_m_bars()
		{ 
			$(menu).show(0);
			$(showHideButton).hide(0);
		}
		
		// show the bar to show/hide menu items on mobile
		function show_mobile_bar()
		{
			//$(menu).addClass('aa-is_mobile');
		}
		
		function window_width()
		{
			return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		}
		
		// unbind events
		function unbind_events()
		{
			$(menu_container).find("li, a").unbind();
			$(document).unbind("click.menu touchstart.menu");
		}
		
		function responsive_menu_toggle()
		{
			menu_container.on("click", ".aa-mega-menu-brand .fa", function(){
				$(menu).slideToggle('fast', function() {
					var $this = $(this);
					
					if( $this.is(':visible') ) {
						menu_container.addClass('aa-mega-menu-open');
					}else{
						menu_container.removeClass('aa-mega-menu-open');
					}
				});
			});
		}
		
		function mega_menu_fix_width()
		{
			var new_width = settings.menuWidthParent.width();
			$(menu).find(".menu-item.menu-mega-menu").find(".aa-mega-sub-menu").width( new_width );
		}

		function init()
		{
			responsive_menu_toggle();
			
			mega_menu_fix_width();
			fix_submenu_right();
			
			if( settings.align == "right" ){
				$(menu).addClass("aa-mega-menu-wrap-right"); 
			}
			
			if(window_width() <= mobileWidthBase && bigScreenFlag > mobileWidthBase){
				settings.trigger = "click";
				unbind_events();
				work_landscape_mode();
			}
			
			if(window_width() > mobileWidthBase && smallScreenFlag <= mobileWidthBase){
				unbind_events();
				hide_m_bars();
				work_landscape_mode();
			}
			
			bigScreenFlag = window_width();
			smallScreenFlag = window_width();

			//$(menu).find(".menu-item.menu-mega-menu > a").click();
		}	
	});
	
	// external usage
	return {
		'settings' : settings
	}
})(jQuery);