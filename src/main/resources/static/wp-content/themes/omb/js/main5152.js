function ombPreloader(elm, fullscreen)
{
	var html = [];
    html.push('<div class="omb-preload '+ (fullscreen == true ? 'omb-preload-fullscreen' : '') +'">');
    html.push(    '<div class="omb-preload-status">');
    html.push(        '<div class="omb-sk-spinner omb-sk-spinner-double-bounce">');
    html.push(            '<div class="omb-sk-double-bounce1"></div>');
    html.push(            '<div class="omb-sk-double-bounce2"></div>');
    html.push(        '</div>');
    html.push(    '</div>');
    html.push('</div>');

    elm.append(html.join("\n"));
}

function ombPreloader_hide( elm ) {
	jQuery(elm).find('.omb-preload').fadeOut(function() {
		jQuery(this).remove();
	});
}
	
;(function($) { "use strict";

	function other_cinemas()
	{
		var other_cinemas = $(".omb-cinemas-carousel");
		other_cinemas.owlCarousel({
		    items : 2,
			margin: 70,
			lazyLoad: true,
		    responsive : {
			    // breakpoint from 0 up
			    0 : {
			        margin: 10,
			    },
			    // breakpoint from 320 up
			    320 : {
			        margin: 30,
			    },
			    // breakpoint from 480 up
			    480 : {
			        margin: 40,
			    },
			}
		});
	
		$(".omb-cinema-others")
			.on('click', '.omb-next', function(e){
				e.preventDefault();
				other_cinemas.trigger('next.owl.carousel');
			})
			.on('click', '.omb-prev', function(e){
				e.preventDefault();
				other_cinemas.trigger('prev.owl.carousel');
			});
	}

	function setOwlStageHeight()
	{
		$(".omb-now-playing-list").each(function() {
			var $that = $(this),
				maxHeight = 0,
				preloader = $that.parents(".omb-now-playing").parent().find('.omb-preload');
				
				$that.parents(".omb-now-playing").show();

				if( preloader.length > 0 ) {
					preloader.hide();
				}
		});
	}

	function now_playing()
	{
		$(".omb-now-playing-list").each(function(){
			var now_playing = $(this),
				wrapper = now_playing.parents(".omb-now-playing").eq(0),
				itemspp = parseInt( now_playing.data('items') ),
				nb = now_playing.find(">div").size();
			
			if( nb > 1 ){
				now_playing.owlCarousel({
				    lazyLoad: true,
				    loop: true,
				    //autoplay: true,
				    //autoplayHoverPause: true,
				    autoHeight : true,
				    onInitialized: setOwlStageHeight,
					onResized: setOwlStageHeight,
					onTranslated: setOwlStageHeight,
					/*responsiveBaseElement: '.omb-now-playing',*/
				    responsiveClass: true,
					responsive : {
					    // breakpoint from 0 up
					    0 : {
					        margin: 0,
					        items : 1
					    },
					    // breakpoint from 320 up
					    320 : {
					        margin: 10,
					        items : 1
					    },
					    // breakpoint from 480 up
					    480 : {
					        margin: 15,
					        items : (itemspp == 1  ? 1 : 2)
					    },
					    // breakpoint from 768 up
					    768 : {
					        margin: 20,
					        items : (itemspp == 1 ? 1 : 3)
					    },
					    // breakpoint from 992 up
					    992 : {
					    	margin: 25,
					        items : itemspp
					    },
					    // breakpoint from 1200 up
					    1200 : {
					        margin: 30,
					        items : itemspp
					    }
					}
				});

				$('body .omb-navigation')
					.on('click', '.omb-next', function(e){
						e.preventDefault();
						now_playing.trigger('next.owl.carousel');
					})
					.on('click', '.omb-prev', function(e){
						e.preventDefault();
						now_playing.trigger('prev.owl.carousel');
					});
			}
		});
	}

	function rating()
	{
		$(".omb-star-rating").each(function(){
			var that 	= $(this),
				stars 	= that.find("span"),
				rating 	= that.data('rating');

			stars.css('width', ( rating * 20 ) + '%');
		});

		$(".omb-rating-container:not(.omb-rating-readonly)").each(function(){
			var that 	= $(this),
				rating 	= that.data('rating'),
				post_id = that.data('postid'),
				stars 	= that.find("span"),
				mark 	= that.find(".omb-rating-container-bk");

			function mark_rating( new_rate )
			{
				mark.find("i").each(function(i){
					var that3 = $(this);
					if( i <= new_rate ){
						that3.attr( 'class', "fa fa-star" ); 
					}else{
						that3.attr( 'class', "fa fa-star-o" ); 
					}
				});

				that.find("input").val( new_rate + 1 );
			}

			mark.on("click", 'i', function(){
				var that2 = $(this),
					index = that2.index();
				mark_rating( index );
			});
		});
	}
	
	function replaceUrlParam(url, paramName, paramValue)
	{
	    var pattern = new RegExp('\\b('+paramName+'=).*?(&|$)')
	    if(url.search(pattern)>=0){
	        return url.replace(pattern,'$1' + paramValue + '$2');
	    }
	    return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue 
	}

	function build_movie_filters()
	{
		$(".omb-movie-filters").each(function(){
			var that = $(this),
				page_url = window.location.href,
				order = that.data('order'),
				order_dir = that.data('orderdir');

			that.find("a#omb-movie-filter-" + order ).addClass("on").addClass("omb-order-dir-" + order_dir);

			that.find("a").each(function(){
				var that2 = $(this),
					new_order = that2.attr('id').replace('omb-movie-filter-', ''),
					new_url = replaceUrlParam(page_url, 'order', that2.attr('id').replace('omb-movie-filter-', '') );

				if( new_order == order ){
					new_url = replaceUrlParam(new_url, 'order_dir', ( order_dir == 'ASC' ? 'desc' : 'asc' ) )
				}

				that2.attr('href', new_url);
			});
		});
	}

	function initOmbMap(map_elm) {
		var geocoder = new google.maps.Geocoder();
		
		var customMapType = new google.maps.StyledMapType([
	      {
	        stylers: [
	          {saturation : (map_elm.data('black_and_white') == true ? -100 : 0)}
	        ]
	      }
	    ], {
	      	name: 'OMB'
	  	});
	  	
	  	var customMapTypeId = 'OMB_style';
	  	 
	  	if( map_elm.data('hide_controls') == true ) {
	  		var map = new google.maps.Map(document.getElementById('OMB-map'), {
				zoom: map_elm.data('zoom'),
				panControl: false,
				zoomControl: false,
				mapTypeControl: false,
				scaleControl: false,
				streetViewControl: false,
				overviewMapControl: false
		  	});
	  	}else{
	  		var map = new google.maps.Map(document.getElementById('OMB-map'), {
				zoom: map_elm.data('zoom')
		  	});
	  	}
	  	
	  	
	  	geocoder.geocode({'address': map_elm.data('address')}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location,
					icon: map_elm.data('marker')
				});
			} else {
				alert('Geocode was not successful for the following reason: ' + status);
			}
		});
		  
		map.mapTypes.set(customMapTypeId, customMapType);
		map.setMapTypeId(customMapTypeId);
	}
	
	$(window).on('load', function() {
		now_playing();
	});
	
	$(document).ready(function(){
		other_cinemas();
		rating();
		build_movie_filters();
		
		// mobile menu
		var menu_container = $('.omb-menu div').eq(1);
		if( menu_container.length > 0 ) {
			var menu = menu_container.find('.menu');
			
			$('.omb-menu-brand').on('click', '.fa-bars', function(e) {
				e.preventDefault();
				
				if( menu_container.hasClass('menu-open') ) {
					menu_container.slideUp(function(){
						$('.omb-menu-brand .fa-bars').css({'opacity': .7});
						menu_container.removeClass('menu-open');
					});
				}else{
					$('.omb-menu-brand .fa-bars').css({'opacity': 1});
					menu_container.slideDown(function(){
						menu_container.addClass('menu-open');
					});
				}
			});

			menu.on('click', 'li.menu-item-has-children > i.fa', function(e) {
				e.preventDefault();

				var that = $(this),
					submenus = that.parent().find('.sub-menu'),
					submenu = submenus.eq(0);

				menu.find('.sub-menu').each(function() {
					var that2 = $(this);

					if( that2.hasClass('submenu-open') && !that.parents().eq(1).hasClass('sub-menu') ) {
						that2.slideUp(function() {
							that2.removeClass('submenu-open');
							that2.prev().removeClass('fa-caret-up');
							that2.prev().addClass('fa-caret-down');
						});
					}
				});

				if( submenu.hasClass('submenu-open') ) {
					submenu.slideUp(function(){
						that.removeClass('fa-caret-up');
						that.addClass('fa-caret-down');
						submenu.removeClass('submenu-open');
					});
				}else{
					submenu.slideDown(function(){
						that.removeClass('fa-caret-down');
						that.addClass('fa-caret-up');
						submenu.addClass('submenu-open');
					});
				}
			});
		}

		$("#movie-slideshow").ombSlideshow({
		    'show_thumbs': true,
		    'number_thumbs': 5
		});
		
		$(".omb-lightbox").ombLightbox({
			'header': {
				'show_header': true,
				'close_btn': true,
				'title': 'Trailer',
				'height': 80
			},
			'overlay_color' : 'rgba(0,0,0, 0.4)',
			'close_btn': false,
	        'content_size' : {
	          'width': '640px',
	          'height': '480px'
	        },
		});
		
		$('body').on('click', "a.omb-show-all-btn", function(e){
			e.preventDefault();
	
			var that = $(this),
				rel_elm = $(that.data('rel')),
				hiddenclass = that.data("hiddenclass");
	
			rel_elm.find(hiddenclass).removeClass( hiddenclass.replace( ".", "" ) );
			that.remove();
		});

		$('.omb-cinema-gallery ul').magnificPopup({
			delegate: 'a',
			type: 'image',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			}
		});

		$('ul.omb-slideshow-main-images').magnificPopup({
			delegate: 'a',
			type: 'image',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			}
		});

		$('.page-content p').magnificPopup({
			delegate: 'a',
			type: 'image'
		});
		
		/*
		 * "BOOK NOW" functionality
		 */
		// Redirect to fullpage reservation if tablet/mobile
		if( $(window).width() <= 992 ) {

			$('body').on('click', ".book-movie, .theatre_booking, .btn_schedule_next", function(e) {
				var $this = $(this);
				
				if( $this.attr('href').indexOf('#') > -1 ) {
					e.preventDefault();
					
					var lightboxData = $this.data('lightbox');

					if( typeof lightboxData != 'object' ) {
						lightboxData = $.parseJSON( lightboxData );
					}
					
					var	movie_id = lightboxData.params && typeof lightboxData.params.movie_id !== undefined ? lightboxData.params.movie_id : '',
						event_id = lightboxData.params && typeof lightboxData.params.event_id !== undefined ? lightboxData.params.event_id : '',
						redirect_url = $this.attr('href').replace('#', '') + '/?full_page_reservation';
						redirect_url += typeof movie_id !== undefined && movie_id > 0 ? '&movie_id=' + movie_id : '',
						redirect_url += typeof event_id !== undefined && event_id > 0 ? '&event_id=' + event_id : '';
					
					window.location.href = redirect_url;
				}
			});

		}else{

			$(".book-movie, .theatre_booking").ombLightbox({
				'header': {
					'show_header': true,
					'close_btn': true,
					'title': 'Reserve your ticket',
					'height': 80
				},
				'overlay_color' : 'rgba(0,0,0, 0.4)',
		        'content_size' : {
		          'width': '1200px',
		          'height': 'auto'
		        }
		        //'onEnd': build_sq_filters
			});

			$('body').on('click', '.btn_schedule_next', function(e){
				var that = $(this);
				
				if( typeof that.attr('href') == 'undefined' || that.attr('href').indexOf('#') > -1 ) {
					e.preventDefault();

					$.ajax(js_vars.ajaxurl, {
		        		'dataType': 'html',
		            	'method': 'POST',
		        		data: {
		            		'action': 'omb_pt_event_ajax_requests',
		            		'sub_action': 'get_hall',
		            		'params': {
		            			show_movie: $('#fullscreen').length > 0 ? false : true,
		            			event_id: that.data("event")
		            		}
		            	}
		        	}).done(function( response ) {
		        		$('#omb-lightbox-content').html( response );
		        		$('#booking-back').show();

		        		if( $('#omb-quick-booking-filters').length > 0 ) {
			        		$('#omb-quick-booking-filters').find('select').each(function(i,k){
								$(this).prop('selectedIndex', 0);
							});
						}

						$('.btn_schedule_next').hide();
		        	});
		        }
			});

		}
		
		$('body').on('click', '#booking-back', function(e){
			e.preventDefault();
			
			$.ajax(js_vars.ajaxurl, {
        		'dataType': 'html',
            	'method': 'POST',
        		data: {
            		'action': 'omb_pt_event_ajax_requests',
            		'sub_action': 'get_pre_reservation',
            		'params': {
            			movie_id: $(this).data('info').movie_id
            		}
            	}
        	}).done(function( response ) {
        		$('#omb-lightbox-content').html( response );
        	});
		});
		
		// Mobile get full page pre reservation
		if( $('#omb-lightbox-content').length > 0 ) {
			var movie_id = $('#omb-lightbox-content').data('info').movie_id > 0 ? $('#omb-lightbox-content').data('info').movie_id : 0,
				event_id = $('#omb-lightbox-content').data('info').event_id > 0 ? $('#omb-lightbox-content').data('info').event_id : 0;

			$.ajax(js_vars.ajaxurl, {
        		'dataType': 'html',
            	'method': 'POST',
        		data: {
            		'action': 'omb_pt_event_ajax_requests',
            		'sub_action': movie_id > 0 ? 'get_pre_reservation' : 'get_hall',
            		'params': {
            			'movie_id': movie_id,
            			'event_id': event_id
            		}
            	}
        	}).done(function( response ) {
        		$('#omb-lightbox-content').html( response );
        	});
		}
		// END Book Now

		// init Quick Booking Widget
		//omb_book_now.init('omb-quick-booking-filters');
		$('#omb-quick-booking-filters').ombBookNow();

		$('.omb-same-height').each( function(){
			var that = $(this);
			function fix_height(){			
				var maxHeight	= 0,
					children	= that.find('>*');
				children.css("height","auto");
				children.each( function(){
					var that2 = $(this),
						height = that2.outerHeight();
					if (height > maxHeight) maxHeight = height;
				});
				children.height( maxHeight );
			}

			$(window).resize(function(){
				fix_height();
			});
			fix_height();

		});

		// init Google Map
		var map_elm = $('div.OMB-map');
		if( map_elm.length > 0 ) {
			initOmbMap(map_elm);
		}

		/*var socialBox = $('.omb_social');
		if( socialBox.length > 0 ) {
			jQuery.post(js_vars.ajaxurl, {
				'action' : 'omb_get_share_count',
				'post_url' : window.location.href
			}, function(resp) { 
				console.log(resp);
				if( resp.tw || resp.fb || resp.gplus ) {
					socialBox.find('.omb-social-twitter .count').html( resp.tw );
					socialBox.find('.omb-social-facebook .count').html( resp.fb );
					socialBox.find('.omb-social-google-plus .count').html( resp.gplus );
				}
			}, 'json');
		}*/

	});

})(jQuery);