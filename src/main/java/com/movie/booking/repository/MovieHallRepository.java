package com.movie.booking.repository;

import com.movie.booking.model.MovieHall;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieHallRepository extends JpaRepository<MovieHall,Long> {

}
