package com.movie.booking.repository;

import com.movie.booking.model.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RatingRepository extends JpaRepository<Rating,Long> {
@Query(value = "select * from rating where user_id=?1 and movie_detail_id=?2 ",nativeQuery = true)
    Rating findByUserAndMovieDetailsOnly(int userId,int movieDetailId);

@Query(value="select round(avg(rating)) from rating where movie_detail_id=?1",nativeQuery = true)
    int ratingValue(long movieDetailId);
}

