package com.movie.booking.repository;

import com.movie.booking.model.Genre;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface GenreRepository extends JpaRepository<Genre,Long> {

    @Query(value="select * from genre",nativeQuery = true)
    List<Genre> getAllGenre();

}
