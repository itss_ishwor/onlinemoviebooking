package com.movie.booking.repository;

import com.movie.booking.model.SeatBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SeatBookRepository extends JpaRepository<SeatBook,Long> {
    @Query(value = "select * from seat_book where user_id=?1",nativeQuery = true)
    List<SeatBook> findBookedSeatByUser(long id);
}
