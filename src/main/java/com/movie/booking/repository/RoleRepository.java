package com.movie.booking.repository;

import com.movie.booking.model.security.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface RoleRepository extends CrudRepository<Role, Integer> {
    Role findByRoleName(String name);

    @Query(value = "select r.role_name from role r, user u,user_role ur where u.user_id=ur.user_id\n" +
            "  and ur.role_id=r.role_id and u.username=?1",nativeQuery = true)
    String getRoleName(String username);
}
