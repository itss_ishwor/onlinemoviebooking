package com.movie.booking.repository;

import com.movie.booking.model.MovieDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface MovieDetailsRepository extends CrudRepository<MovieDetails,Long> {
    @Query(value = "select * from movie_details where id=?1",nativeQuery = true)
    MovieDetails findByMovieDetailsID(long id);
}
