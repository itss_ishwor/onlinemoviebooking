package com.movie.booking;

import com.movie.booking.model.security.UserRole;
import com.movie.booking.service.UserService;
import com.movie.booking.model.User;
import com.movie.booking.model.security.Role;
import com.movie.booking.utility.SecurityUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashSet;
import java.util.Set;


@SpringBootApplication
public class movieBookingApplication implements CommandLineRunner{

    @Autowired
    private UserService userService;

    public static void main(String[] args){

        SpringApplication.run(movieBookingApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        User user = new User();
        user.setFirstName("Admin");
        user.setMiddleName("admin");
        user.setLastName("Admin");
        user.setUsername("admin1");
        user.setPassword(SecurityUtility.passwordEncoder().encode("admin"));
        user.setEmail("admin1@gmail.com");
        Set<UserRole> userRoles = new HashSet<>();
        Role role = new Role();
        role.setId(2);
        role.setRoleName("ROLE_ADMIN");
        userRoles.add(new UserRole(user, role));

        userService.createUser(user,userRoles);
    }
}
