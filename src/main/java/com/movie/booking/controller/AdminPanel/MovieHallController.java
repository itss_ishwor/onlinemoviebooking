package com.movie.booking.controller.AdminPanel;

import com.movie.booking.model.Movie;
import com.movie.booking.model.MovieDetailToHall;
import com.movie.booking.model.MovieDetails;
import com.movie.booking.model.MovieHall;
import com.movie.booking.service.MovieDetailsService;
import com.movie.booking.service.MovieHallService;
import com.movie.booking.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class MovieHallController {
    @Autowired
    private MovieHallService movieHallService;
    @Autowired
    private MovieService movieService;
    @Autowired
    private MovieDetailsService movieDetailsService;

    @GetMapping(value = "/add-hall")
    public String getMovieHall() {
        return "admintemplates/hallsection/addmoviehall";
    }

    @PostMapping(value = "/add/hall")
    public String saveHall(@ModelAttribute MovieHall movieHall, RedirectAttributes redirectAttributes) {
        System.out.println("i ammmmmmmm" + movieHall.getHallName() + movieHall.getCapacity());
        movieHallService.save(movieHall);
        redirectAttributes.addFlashAttribute("success", "Data Successfully Saved");
        return "redirect:/add-hall";
    }

    @GetMapping(value = "/view/movie/hall")
    public String viewMovieHalls(Model model) {
        model.addAttribute("hallList", movieHallService.findAll());
        return "admintemplates/hallsection/viewmoviehall";
    }

    @PostMapping(value = "/edit/movie/hall")
    public String editMovieHall(@ModelAttribute MovieHall movieHall) {
        System.out.println("movie" + movieHall);
        MovieHall movieHall1 = movieHallService.findById(movieHall.getId());
        movieHall1.setCapacity(movieHall.getCapacity());
        movieHall1.setHallName(movieHall.getHallName());
        System.out.println("movie" + movieHall1);
        movieHallService.save(movieHall1);
        return "redirect:/view/movie/hall";
    }

    @GetMapping(value = "/delete/hall/{id}")
    public String delHall(@PathVariable("id") long id) {
        System.out.println("id is" + id);
        try {
            movieHallService.deleteMovieHall(id);
        } catch (Exception e) {
            System.out.println("Error :" + e.getMessage());
        }

        return "redirect:/view/movie/hall";
    }

    @GetMapping(value = "/detail/hall/{id}")
    public String detailHall(@PathVariable("id") long id, Model model) {
        System.out.println("id is" + id);
        Movie movie = movieService.findOne(id);
        List<MovieHall> movieHallList = movieHallService.findAll();
        model.addAttribute("movie", movie);
        model.addAttribute("movieHallList", movieHallList);
        return "admintemplates/moviesection/addDetailmovie";
    }

    @PostMapping(value = "/add/moviedetail/movieid={movieid}")
    public String addMovieDetail(@PathVariable("movieid") long movieid, @ModelAttribute MovieDetails movieDetails) {
        Movie movie = movieService.findOne(movieid);
        MovieDetails movieDetails1 = new MovieDetails(movieDetails.getShowDate(), movieDetails.getTime(), movieDetails.getPrice(), movieDetails.getMovieHall(), movie);
        movieDetailsService.saveMovieDetails(movieDetails1);
        System.out.println("Here" + movieDetails1.getShowDate() + " " + movieDetails1.getTime() + " " + movieDetails1.getPrice() + " " + movieDetails1.getMovie().getId() + " " + movieDetails1.getMovieHall().getHallName());


        return "redirect:/detail/hall/" + movie.getId();
    }
}
