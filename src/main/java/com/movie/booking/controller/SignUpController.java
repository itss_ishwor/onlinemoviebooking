//package com.movie.booking.controller;
//
//import com.google.gson.Gson;
//import com.movie.booking.model.security.PasswordResetToken;
//import com.movie.booking.model.security.UserRole;
//import com.movie.booking.service.impl.UserSecurityService;
//import com.movie.booking.service.UserService;
//import com.movie.booking.utility.MailConstructor;
//import com.movie.booking.model.User;
//import com.movie.booking.model.security.Role;
//import com.movie.booking.utility.SecurityUtility;
//import org.apache.catalina.servlet4preview.http.HttpServletRequest;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mail.SimpleMailMessage;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.HashSet;
//import java.util.Locale;
//import java.util.Set;
//import java.util.UUID;
//
//@Controller
//public class SignUpController {
//    @Autowired
//    private UserService userService;
//
//    @Autowired
//    private JavaMailSender mailSender;
//
//    @Autowired
//    private MailConstructor mailConstructor;
//
//    @Autowired
//    private UserSecurityService userSecurityService;
//
//    /*@GetMapping(value = "/signUp")
//    public String getSignUp() {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if (!(authentication instanceof AnonymousAuthenticationToken)) {
//            return "redirect:/home";
//        } else {
//            return "signUp";
//        }
//    }*/
//
//    @RequestMapping(value = "/newUser", method = RequestMethod.POST)
//    public String newUserPost(HttpServletRequest request, @ModelAttribute("email") String userEmail,
//                              @ModelAttribute("username") String username, Model model) throws Exception {
//        model.addAttribute("classActiveNewAccount", true);
//        model.addAttribute("email", userEmail);
//        model.addAttribute("username", username);
//
//        if (userService.findByUsername(username) != null) {
//            model.addAttribute("usernameExists", true);
//
//            return "login";
//        }
//
//        if (userService.findByEmail(userEmail) != null) {
//            model.addAttribute("emailExists", true);
//
//            return "login";
//        }
//
//        User user = new User();
//        user.setUsername(username);
//        user.setEmail(userEmail);
//
//        String password = SecurityUtility.randomPassword();
//
//        String encryptedPassword = SecurityUtility.passwordEncoder().encode(password);
//
//        user.setPassword(encryptedPassword);
//
//        Role role = new Role();
//        role.setId(1);
//        role.setRoleName("ROLE_USER");
//        Set<UserRole> userRoles = new HashSet<>();
//        userRoles.add(new UserRole(user, role));
//
//        userService.createUser(user, userRoles);
//
//        String token = UUID.randomUUID().toString();
//        userService.createPasswordResetTokenForUser(user, token);
//
//        String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
//        //String appUrl = "http://" + "192.168.0.101" + ":" + "8081" + request.getContextPath();
//
//        SimpleMailMessage email = mailConstructor.constructResetTokenEmail(appUrl, request.getLocale(), token, user,
//                password);
//
//        System.out.println("The mail is::" + appUrl);
//        System.out.println("The email is::"+email);
//        mailSender.send(email);
//
//        model.addAttribute("emailSent", true);
//
//        return "login";
//    }
//
//    @RequestMapping("/newUser")
//    public String newUser(Locale locale, @RequestParam("token") String token, Model model){
//        PasswordResetToken passToken = userService.getPasswordResetToken(token);
//
//        if(passToken == null){
//            String message = "Invalid Token.";
//            model.addAttribute("message",message);
//            return "redirect:/badRequest";
//        }
//
//        User user = passToken.getUser();
//        String username = user.getUsername();
//        System.out.println("THe username is:::"+user.getUsername());
//
//        UserDetails userDetails = userSecurityService.loadUserByUsername(username);
//
//        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(),userDetails.getAuthorities());
//
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//        model.addAttribute("user",user);
//        model.addAttribute("classActiveEdit",true);
//        return "MyProfileController";
//    }
//
//    @RequestMapping(value="/updateUserInfo", method = RequestMethod.POST)
//    public String updateUserInfo(
//            @ModelAttribute("user") User user,
//            @ModelAttribute("newPassword") String newPassword,
//            Model model
//    )throws Exception{
//        System.out.println("The user:::"+new Gson().toJson(user));
//        User currentUser = userService.findById(user.getId());
//
//        if(currentUser == null){
//            throw new Exception ("User not found");
//        }
//
//        /* Check email already exists*/
//        if(userService.findByEmail(user.getEmail()) !=null){
//            if(userService.findByEmail(user.getEmail()).getId() != currentUser.getId()){
//                model.addAttribute("emailExists",true);
//                return "MyProfileController";
//            }
//        }
//
//        /* Check username already exists */
//        if(userService.findByUsername(user.getUsername()) != null){
//            if(userService.findByUsername(user.getUsername()).getId() != currentUser.getId()){
//                model.addAttribute("usernameExists",true);
//                return "MyProfileController";
//            }
//        }
//        System.out.println("The new PW is:::"+newPassword);
//        System.out.println("The old password is:::"+currentUser.getPassword());
//        /* Update Password */
//        if(newPassword != null && !newPassword.isEmpty() && !newPassword.equals("")){
//            BCryptPasswordEncoder passwordEncoder = SecurityUtility.passwordEncoder();
//            System.out.println("The password encoder:::"+passwordEncoder);
//            String dbPassword = currentUser.getPassword();
//            if(passwordEncoder.matches(currentUser.getPassword(),dbPassword)){
//                currentUser.setPassword(passwordEncoder.encode(newPassword));
//            }else{
//                model.addAttribute("incorrectPassword",true);
//                return "MyProfileController";
//            }
//        }
//
//        currentUser.setFirstName(user.getFirstName());
//        currentUser.setMiddleName(user.getMiddleName());
//        currentUser.setLastName(user.getLastName());
//        currentUser.setUsername(user.getUsername());
//        currentUser.setEmail(user.getEmail());
//        System.out.println(new Gson().toJson(currentUser));
//        userService.save(currentUser);
//
//        model.addAttribute("updateSuccess", true);
//        model.addAttribute("user", currentUser);
//        model.addAttribute("classActiveEdit", true);
//
//        model.addAttribute("listOfShippingAddresses", true);
//        model.addAttribute("listOfCreditCards", true);
//
//        UserDetails userDetails = userSecurityService.loadUserByUsername(currentUser.getUsername());
//        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(),
//                userDetails.getAuthorities());
//
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//
//        return "MyProfileController";
//    }
//
//
//}
