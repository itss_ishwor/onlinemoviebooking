//package com.movie.booking.controller.userPanel;
//
//import com.movie.booking.model.User;
//import com.movie.booking.model.UserPayment;
//import com.movie.booking.service.UserService;
//import com.movie.booking.utility.LocationConstants;
//import org.apache.catalina.servlet4preview.http.HttpServletRequest;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import java.security.Principal;
//import java.util.Collections;
//import java.util.List;
//
//@Controller
//public class MyProfileController {
//
//    @Autowired
//    private UserService userService;
//
//    @RequestMapping("/myProfile")
//    public String myProfile(Model model, Principal principal) {
//        User user = userService.findByUsername(principal.getName());
//        model.addAttribute("user", user);
//        model.addAttribute("userPaymentList", user.getUserPaymentList());
//        model.addAttribute("bookingList", user.getBookingList());
//
//        model.addAttribute("listOfCreditCards", true);
//        model.addAttribute("listOfShippingAddresses", true);
//
//        List<String> stateList = LocationConstants.listOfUSStatesCode;
//        Collections.sort(stateList);
//        model.addAttribute("stateList", stateList);
//        model.addAttribute("classActiveEdit", true);
//
//        return "myAccount/myProfile";
//    }
//
//    @RequestMapping("/addNewCreditCard")
//    public String addNewCreditCard(
//            Model model, Principal principal
//    ){
//        User user = userService.findByUsername(principal.getName());
//        model.addAttribute("user", user);
//
//        model.addAttribute("addNewCreditCard", true);
//        model.addAttribute("classActiveBilling", true);
//        model.addAttribute("listOfShippingAddresses", true);
//
//        UserPayment userPayment = new UserPayment();
//
//        model.addAttribute("userPayment", userPayment);
//
//        List<String> stateList = LocationConstants.listOfUSStatesCode;
//        Collections.sort(stateList);
//        model.addAttribute("stateList", stateList);
//        model.addAttribute("userPaymentList", user.getUserPaymentList());
//        model.addAttribute("orderList", user.getBookingList());
//
//        return "myAccount/myProfile";
//    }
//
//    @PostMapping(value="/addNewCreditCard")
//    public String addNewCreditCard(
//            @ModelAttribute("userPayment") UserPayment userPayment,
//            Principal principal, Model model
//    ){
//        User user = userService.findByUsername(principal.getName());
//        userService.updateUserBilling(userPayment, user);
//
//        model.addAttribute("user", user);
//        model.addAttribute("userPaymentList", user.getUserPaymentList());
//        model.addAttribute("listOfCreditCards", true);
//        model.addAttribute("classActiveBilling", true);
//        model.addAttribute("listOfShippingAddresses", true);
//
//        return "myAccount/myProfile";
//    }
//
//    @RequestMapping("/listOfCreditCards")
//    public String listOfCreditCards(
//            Model model, Principal principal, HttpServletRequest request
//    ) {
//        User user = userService.findByUsername(principal.getName());
//        model.addAttribute("user", user);
//        model.addAttribute("userPaymentList", user.getUserPaymentList());
//        model.addAttribute("orderList", user.getBookingList());
//
//        model.addAttribute("listOfCreditCards", true);
//        model.addAttribute("classActiveBilling", true);
//
//        return "myAccount/myProfile";
//    }
//
//    @PostMapping(value="/setDefaultPayment")
//    public String setDefaultPayment(
//            @ModelAttribute("defaultUserPaymentId") Long defaultPaymentId, Principal principal, Model model
//    ) {
//        User user = userService.findByUsername(principal.getName());
//        userService.setUserDefaultPayment(defaultPaymentId, user);
//
//        model.addAttribute("user", user);
//        model.addAttribute("listOfCreditCards", true);
//        model.addAttribute("classActiveBilling", true);
//        model.addAttribute("listOfShippingAddresses", true);
//
//        model.addAttribute("userPaymentList", user.getUserPaymentList());
//        model.addAttribute("orderList", user.getBookingList());
//
//        return "myAccount/myProfile";
//    }
//
//}
