package com.movie.booking.controller.userPanel;

import com.movie.booking.model.*;
import com.movie.booking.repository.MovieDetailsRepository;
import com.movie.booking.repository.MovieRepository;
import com.movie.booking.repository.RatingRepository;
import com.movie.booking.repository.SeatBookRepository;
import com.movie.booking.service.*;
import com.movie.booking.utility.LocationConstants;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.security.Principal;
import java.sql.Date;
import java.util.Collections;
import java.util.List;

@Controller
public class MovieScheduleController {
    @Autowired
    private UserService userService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieHallService movieHallService;

    @Autowired
    private MovieDetailsRepository movieDetailsRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private SeatBookRepository seatBookRepository;

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private PaymentService paymentService;
    private Payment payment = new Payment();

    @GetMapping(value = "/movie-schedule")
    public String viewMovieSchedule(Model model, Principal principal){
        if(principal != null) {
            String username = principal.getName();
            User user = userService.findByUsername(username);
            model.addAttribute("user", user);
        }

        model.addAttribute("movieList", movieRepository.findMovieToShows());
        model.addAttribute("moviehall", movieHallService.findAll());
        model.addAttribute("activeAll",true);
        return "movie/movieschedule";

    }

    @GetMapping(value = "/contactus")
    public String getMovieContact(){
        return "contactus";
    }

    @GetMapping(value = "/book-movie-{detailid}")
    public String bookMovie(@PathVariable("detailid") long id, Model model,Principal principal){
        User user = userService.findByUsername(principal.getName());
        List<UserPayment> userPaymentList = user.getUserPaymentList();
        model.addAttribute("userPaymentList", userPaymentList);
        if (userPaymentList.size() == 0) {
            model.addAttribute("emptyPaymentList", true);
        } else {
            model.addAttribute("emptyPaymentList", false);
        }
        List<String> stateList = LocationConstants.listOfUSStatesCode;
        Collections.sort(stateList);
        model.addAttribute("stateList", stateList);
        for (UserPayment userPayment : userPaymentList) {
            if (userPayment.isDefaultPayment()) {
                paymentService.setByUserPayment(userPayment, payment);
            }
        }

        model.addAttribute("payment", payment);
        MovieDetails movieDetails=movieDetailsRepository.findByMovieDetailsID(id);
        model.addAttribute("userPaymentList", user.getUserPaymentList());
        model.addAttribute("bookmovie",movieDetails);
        return "movie/bookmovie";
    }

    @PostMapping(value = "/book-movie-{detailid}")
    public String setBookMovie(@PathVariable("detailid") String id, Model model, Principal principal,
                               @ModelAttribute("payment") Payment payment, @RequestParam("setTotal")BigDecimal total){
        User user = userService.findByUsername(principal.getName());
        List<UserPayment> userPaymentList = user.getUserPaymentList();
        model.addAttribute("userPaymentList", userPaymentList);
        if (userPaymentList.size() == 0) {
            model.addAttribute("emptyPaymentList", true);
        } else {
            model.addAttribute("emptyPaymentList", false);
        }
        List<String> stateList = LocationConstants.listOfUSStatesCode;
        Collections.sort(stateList);
        model.addAttribute("stateList", stateList);
        for (UserPayment userPayment : userPaymentList) {
            if (userPayment.isDefaultPayment()) {
                paymentService.setByUserPayment(userPayment, payment);
            }
        }
        Payment us = payment;
        System.out.println(us.getType());
        System.out.println(us.getHolderName());
        System.out.println(us.getCvc());
        System.out.println(us.getCardNumber());
        System.out.println(us.getExpiryYear());
        System.out.println(us.getExpiryMonth());
        System.out.println(total);
        //userService.updateUserBilling(us, user);
        model.addAttribute("userPaymentList", user.getUserPaymentList());
        Booking booking = bookingService.createBooking(us,user,total);

        /*mailSender.send(mailConstructor.constructOrderConfirmationEmail(user, order, Locale.ENGLISH));*/

        model.addAttribute("payment", payment);
        Long movieId = Long.valueOf(id);
        MovieDetails movieDetails=movieDetailsRepository.findByMovieDetailsID(movieId);
        model.addAttribute("bookmovie",movieDetails);
        return "movie/bookmovie";
    }

    @GetMapping(value = "/booked/seats")
    public ResponseEntity<?> getBookedSeats(@RequestParam("seats") String [] seats,
                                            @RequestParam("movieid") long id,
                                            Principal principal,Model model){

       User user=new User() ;
       if(principal != null) {
            String username = principal.getName();
           user= userService.findByUsername(username);
            model.addAttribute("user", user);
        }
        System.out.println(user.getId());
        System.out.println("movie id is"+id);

       for(String seatName:seats){
           seatName=seatName.replaceAll("\"","");
           seatName=seatName.replaceAll("\\[|\\]","");
           System.out.println("seats are"+seatName);
           SeatBook seatBook=new SeatBook(seatName,new MovieDetails(id),user);
           seatBookRepository.save(seatBook);
       }

        return ResponseEntity.ok("success");
    }
    @GetMapping(value = "/movie-myshow")
    public String showMyshows(Model model,Principal principal){

        if(principal != null) {
            String username = principal.getName();
            User user = userService.findByUsername(username);
            model.addAttribute("user", user);

            model.addAttribute("seatlist",seatBookRepository.findBookedSeatByUser(user.getId()));
        }
        return "movie/myshow";
    }

    @GetMapping(value="/rate/movie")
    public ResponseEntity<?> rateMovie(@RequestParam("rating") int rating,
                                       @RequestParam("movieid") int movieId,Principal principal){
       User user=new User();
        if(principal != null) {
            String username = principal.getName();
            user= userService.findByUsername(username);

        }
            Rating rating1=ratingRepository.findByUserAndMovieDetailsOnly((int) user.getId(),movieId);

        if(rating1==null) {
            ratingRepository.save(new Rating(rating, new MovieDetails(movieId), user));
            System.out.println("rating is" + rating + movieId);
        }else{
            System.out.println("already inserted    ");
            rating1.setRating(rating);
            ratingRepository.save(rating1);

        }

        return  ResponseEntity.ok("success");
    }
}
