package com.movie.booking.controller;


import com.movie.booking.model.News;
import com.movie.booking.model.User;
import com.movie.booking.repository.NewsRepository;
import com.movie.booking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.Principal;

@Controller
public class NewsController {

    @Autowired
    private UserService userService;

    @Autowired
    private NewsRepository newsRepository;
    @GetMapping(value = "/add/news")
    public String getNews(Model model, Principal principal){
        User user =new User();
        if(principal != null) {
            String username = principal.getName();
            user = userService.findByUsername(username);

            model.addAttribute("user", user);
        }
        return "/news/addnews";
    }

    @PostMapping(value="/add/news")
    public String  saveHall(@ModelAttribute News news, HttpServletRequest request, RedirectAttributes redirectAttributes){

        System.out.println(news.toString());

      newsRepository.save(news);

        MultipartFile newsImage = news.getNewsImage();

        try {
            byte[] bytes = newsImage.getBytes();
            String name = news.getId() + ".png";
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(new File("src/main/resources/static/images/news/" + name)));
            stream.write(bytes);
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        redirectAttributes.addFlashAttribute("success","Data Successfully Saved");

        return "redirect:/add/news";
    }

    @GetMapping(value = "/view/news")
    public String viewMovieHalls(Model model) {
        model.addAttribute("newslist", newsRepository.findAll());

        return "news/viewnews";
    }


    @PostMapping(value = "/edit/news")
    public String editMovie(@ModelAttribute News news) {

        News news1 = newsRepository.findOne(news.getId());
        news1.setTitle(news.getTitle());
        news1.setDescription(news.getDescription());
        newsRepository.save(news1);
        return "redirect:/view/news";
    }

    @GetMapping(value = "/delete/news/{id}")
    public String delMovie(@PathVariable("id") long id) {
        System.out.println("id is" + id);
        try {
            newsRepository.delete(id);
        } catch (Exception e) {
            System.out.println("Error :" + e.getMessage());
        }

        return "redirect:/view/news";
    }

    @GetMapping(value="/news-blog")
    public String showNewsBlog(Model model){
        model.addAttribute("newslist", newsRepository.findAll());
        return "newspanel";
    }

    @GetMapping(value="/single-blog-{id}")
    public String showSingleBlog(Model model, @PathVariable("id") long id){
        model.addAttribute("news", newsRepository.findOne(id));
        model.addAttribute("newslist", newsRepository.findAll());
        return "singlenews";
    }
}
