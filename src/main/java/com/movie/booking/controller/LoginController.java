//package com.movie.booking.controller;
//
//import com.movie.booking.model.User;
//import com.movie.booking.service.UserService;
//import com.movie.booking.utility.MailConstructor;
//import com.movie.booking.utility.SecurityUtility;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mail.SimpleMailMessage;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.UUID;
//
//@Controller
//public class LoginController {
//
//    @Autowired
//    private UserService userService;
//
//    @Autowired
//    private MailConstructor mailConstructor;
//
//    @Autowired
//    private JavaMailSender mailSender;
//
//    /*@GetMapping(value = "/login")
//    public ModelAndView login(){
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//
//        if(!(authentication instanceof AnonymousAuthenticationToken)){
//            return new ModelAndView("redirect:/home");
//        }else{
//            ModelAndView modelAndView = new ModelAndView();
//            modelAndView.setViewName("login");
//            return modelAndView;
//        }
//    }*/
//
//    @RequestMapping("/login")
//    public String login(Model model){
//        model.addAttribute("classActiveLogin", true);
//        return "login";
//    }
//
//    @RequestMapping("/forgetPassword")
//    public String forgetPassword(HttpServletRequest request, @ModelAttribute("email") String email, Model model){
//        model.addAttribute("classActiveForgetPassword",true);
//
//        User user = userService.findByEmail(email);
//
//        if(user == null){
//            model.addAttribute("emailNotExists", true);
//            return "login";
//        }
//        String password = SecurityUtility.randomPassword();
//
//        String encryptedPassword = SecurityUtility.passwordEncoder().encode(password);
//
//        user.setPassword(encryptedPassword);
//
//        userService.save(user);
//
//        String token = UUID.randomUUID().toString();
//        userService.createPasswordResetTokenForUser(user,token);
//
//        String appUrl = "http://" + request.getServerName()+":"+request.getServerPort()+request.getContextPath();
//        SimpleMailMessage newEmail = mailConstructor.constructResetTokenEmail(appUrl,request.getLocale(),token,user,password);
//
//        mailSender.send(newEmail);
//
//        model.addAttribute("forgetPasswordEmailSent",true);
//
//        return "login";
//    }
//}
