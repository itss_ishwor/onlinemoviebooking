package com.movie.booking.model;

import javax.persistence.*;

@Entity
@Table(name="rating")
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int rating;
    @ManyToOne
    @JoinColumn(name = "movie_detail_id")
    private MovieDetails movieDetails;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;


    public Rating() {
    }

    public Rating(int rating, MovieDetails movieDetails, User user) {
        this.rating = rating;
        this.movieDetails = movieDetails;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public MovieDetails getMovieDetails() {
        return movieDetails;
    }

    public void setMovieDetails(MovieDetails movieDetails) {
        this.movieDetails = movieDetails;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
