package com.movie.booking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieHall {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String hallName;

    private int capacity;

    @OneToMany(mappedBy = "movieHall",cascade = CascadeType.ALL)
    @JsonManagedReference(value = "moviehall_manage")
    //@JsonIgnore
    private List<MovieDetails> movieDetails;
}
