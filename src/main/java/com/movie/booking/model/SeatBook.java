package com.movie.booking.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SeatBook {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
     private String seatName;
    @ManyToOne
    @JoinColumn(name = "movie_detail_id")
     private MovieDetails movieDetails;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public SeatBook(String seatName, MovieDetails movieDetails, User user) {
        this.seatName = seatName;
        this.movieDetails = movieDetails;
        this.user = user;
    }
}
