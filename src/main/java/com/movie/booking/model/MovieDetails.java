package com.movie.booking.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Date showDate;
    private String time;
    private double price;
    @ManyToOne
    @JoinColumn(name = "moviehall_id")
    private MovieHall movieHall;

    @ManyToOne
    @JoinColumn(name = "movie_id")
    @JsonBackReference(value = "moviehall_manage")
    private Movie movie;

    @OneToMany(mappedBy = "movieDetails",cascade = CascadeType.ALL)
    @JsonManagedReference(value = "movies_seat_books")
    //@JsonIgnore
    private List<SeatBook> seatBooks;

    public MovieDetails(Date showDate, String time, double price, MovieHall movieHall, Movie movie) {
        this.showDate = showDate;
        this.time = time;
        this.price = price;
        this.movieHall = movieHall;
        this.movie = movie;
    }
    public MovieDetails(long id){
        this.id=id;

    }
}
