package com.movie.booking.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieDetailToHall {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "movie_details_id")
    private MovieDetails movieDetails;

    @ManyToOne
    @JoinColumn(name = "movie_hall_id")
    private MovieHall movieHall;

    public MovieDetailToHall(MovieDetails movieDetails, MovieHall movieHall) {
        this.movieDetails = movieDetails;
        this.movieHall = movieHall;
    }
}
