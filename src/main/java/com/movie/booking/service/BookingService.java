package com.movie.booking.service;

import com.movie.booking.model.Booking;
import com.movie.booking.model.Payment;
import com.movie.booking.model.User;

import java.math.BigDecimal;

public interface BookingService {
    Booking createBooking(Payment payment, User user, BigDecimal total);

    Booking findOne(Long id);
}
