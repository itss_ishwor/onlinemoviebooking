package com.movie.booking.service.impl;

import com.movie.booking.model.*;
import com.movie.booking.repository.BookingRepository;
import com.movie.booking.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    private BookingRepository bookingRepository;

    @Override
    public Booking createBooking(Payment payment, User user,BigDecimal total) {
        Booking booking = new Booking();
        booking.setPayment(payment);
        booking.setBookingTotal(total);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        String d = "2018-01-01";
        try {
            date = df.parse(d);
        } catch (Exception e) {
            e.printStackTrace();
            date = null;
        }

        booking.setBookingDate(date);
        booking.setUser(user);

        /*List<CartItem> cartItemList = cartItemService.findByShoppingCart(shoppingCart);

        for(CartItem cartItem : cartItemList) {
            Book book = cartItem.getBook();
            cartItem.setOrder(order);
            book.setInStockNumber(book.getInStockNumber() - cartItem.getQty());
        }

        order.setCartItemList(cartItemList);
        order.setOrderDate(Calendar.getInstance().getTime());
        order.setOrderTotal(shoppingCart.getGrandTotal());
        shippingAddress.setOrder(order);
        billingAddress.setOrder(order);
        payment.setOrder(order);
        order.setUser(user);*/
        booking = bookingRepository.save(booking);
        return booking;
    }

    @Override
    public Booking findOne(Long id) {
        return bookingRepository.findOne(id);
    }
}
