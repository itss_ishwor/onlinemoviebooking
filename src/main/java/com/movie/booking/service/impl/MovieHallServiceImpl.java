package com.movie.booking.service.impl;

import com.movie.booking.model.MovieHall;
import com.movie.booking.repository.MovieHallRepository;
import com.movie.booking.service.MovieHallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class MovieHallServiceImpl implements MovieHallService {
    @Autowired
    private MovieHallRepository movieHallRepository;
    @Override
    public void save(MovieHall movieHall) {
        movieHallRepository.save(movieHall);
    }

    @Override
    public List<MovieHall> findAll() {
        return movieHallRepository.findAll();
    }

    @Override
    public void deleteMovieHall(long id) {
        movieHallRepository.delete(id);
    }

    @Override
    public MovieHall findById(long id) {
        return movieHallRepository.findOne(id);
    }
}
