package com.movie.booking.service.impl;

import com.movie.booking.model.Genre;
import com.movie.booking.repository.GenreRepository;
import com.movie.booking.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {
    @Autowired
    private GenreRepository genreRepository;

    @Override
    public Genre findById(Long id) {
        return genreRepository.findOne(id);
    }

    @Override
    public List<Genre> findAll() {
        return genreRepository.findAll();
    }
}
