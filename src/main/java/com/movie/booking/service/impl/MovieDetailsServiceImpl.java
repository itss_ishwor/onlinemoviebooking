package com.movie.booking.service.impl;

import com.movie.booking.model.MovieDetailToHall;
import com.movie.booking.model.MovieDetails;
import com.movie.booking.repository.MovieDetailsRepository;
import com.movie.booking.service.MovieDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class MovieDetailsServiceImpl implements MovieDetailsService{

    @Autowired
    private MovieDetailsRepository movieDetailsRepository;

    @Override
    public MovieDetails saveMovieDetails(MovieDetails movieDetails) {
        return movieDetailsRepository.save(movieDetails);
    }
}
