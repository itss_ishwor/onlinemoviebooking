package com.movie.booking.service.impl;

import com.movie.booking.model.Movie;

import com.movie.booking.repository.MovieRepository;
import com.movie.booking.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class MovieServiceImpl implements MovieService {
    @Autowired
    private MovieRepository movieRepository;

    @Override
    public List<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie findOne(Long id) {
        return movieRepository.findOne(id);
    }



    @Override
    public Long save(Movie movie) {
        Movie movie1=movieRepository.saveAndFlush(movie);
        return movie1.getId();
    }

    @Override
    public void delete(long id) {
        movieRepository.delete(id);
    }


    @Override
    public List<Movie> findByGenreType(long id) {
        return movieRepository.findByGenreType(id);
    }

    @Override
    public List<Movie> findByMovieName(String movieName) {
        return movieRepository.findByMovieName(movieName);
    }



}


