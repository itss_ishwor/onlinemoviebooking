package com.movie.booking.service;

import com.movie.booking.model.Genre;

import java.util.List;

public interface GenreService {
    Genre findById(Long id);
    List<Genre> findAll();
}
