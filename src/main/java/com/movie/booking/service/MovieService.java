package com.movie.booking.service;

import com.movie.booking.model.Movie;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public interface MovieService {
    List<Movie> findAll();
    Movie findOne(Long id);
    Long save(Movie movie);
    void delete(long id);

    List<Movie> findByGenreType(long id);
    List<Movie> findByMovieName(String movieName);


}
