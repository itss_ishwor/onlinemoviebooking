package com.movie.booking.service;

import com.movie.booking.model.Payment;
import com.movie.booking.model.UserPayment;

public interface PaymentService {
    Payment setByUserPayment(UserPayment userPayment, Payment payment);
}
