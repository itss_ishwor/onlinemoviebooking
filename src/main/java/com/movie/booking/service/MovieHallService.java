package com.movie.booking.service;

import com.movie.booking.model.MovieHall;

import java.util.List;

public interface MovieHallService {
    void save(MovieHall movieHall);
    List<MovieHall> findAll();
    void deleteMovieHall(long id);
    MovieHall findById(long id);

}
