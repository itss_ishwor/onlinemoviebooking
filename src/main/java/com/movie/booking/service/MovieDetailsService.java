package com.movie.booking.service;

import com.movie.booking.model.MovieDetailToHall;
import com.movie.booking.model.MovieDetails;

import java.util.Set;

public interface MovieDetailsService {

    MovieDetails saveMovieDetails(MovieDetails movieDetails);
}
